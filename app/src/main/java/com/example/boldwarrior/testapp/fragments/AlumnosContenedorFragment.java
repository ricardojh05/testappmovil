package com.example.boldwarrior.testapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.boldwarrior.testapp.Api;
import com.example.boldwarrior.testapp.R;
import com.example.boldwarrior.testapp.activities.AlumnosActivity;
import com.example.boldwarrior.testapp.models.Alumnos;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AlumnosContenedorFragment extends Fragment{

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_contenedor_alumnos, container, false);

        final ListView listViewAlumnos = (ListView) rootView.findViewById(R.id.listViewAlumnos);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api= retrofit.create(Api.class);
        Call<List<Alumnos>> call = api.getAlumnos();
        call.enqueue(new Callback<List<Alumnos>>() {
            @Override
            public void onResponse(Call<List<Alumnos>> call, Response<List<Alumnos>> response) {
                List<Alumnos> alumnos=response.body();

                String [] alumnosName=new  String[alumnos.size()];
                for (int i=0; i<alumnos.size();i++){
                    alumnosName[i]=alumnos.get(i).getNombre();
                }

                listViewAlumnos.setAdapter(
                        new ArrayAdapter<String>(
                                getActivity(),
                                android.R.layout.simple_list_item_1,
                                alumnosName
                        )
                );

                for (Alumnos h: alumnos){
                    Log.d("nombre",h.getNombre());
                      Log.d("apellido",h.getApellido());
                       //Log.d("id_clase",h.getClaseId());
                    }

//                for (Alumnos h: alumnos){
//                    Log.d("id",h.getClaseId());
//                }

                listViewAlumnos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    if (position==0){
                                        Intent intent =new Intent(getActivity(), AlumnosActivity.class);
                                        startActivity(intent);

//                                        Toast.makeText(getActivity(),"Calificar opcion",Toast.LENGTH_SHORT).show();

                                    }else if (position==1){
                                        Toast.makeText(getActivity(),"Segunda opcion",Toast.LENGTH_SHORT).show();
                                    }else  if(position ==2){
                                        Toast.makeText(getActivity(),"Tercera opcion",Toast.LENGTH_SHORT).show();
                                    }
                    }
                });

            }
            @Override
            public void onFailure(Call<List<Alumnos>> call, Throwable t) {
                            Toast.makeText(getActivity(), t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });


        return rootView;
    }




}
