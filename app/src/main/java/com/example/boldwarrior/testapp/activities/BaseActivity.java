package com.example.boldwarrior.testapp.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;



public abstract class BaseActivity extends AppCompatActivity  {
    private ProgressDialog pDialog;

    public final static int SOLICITUD_FINALIZADA_OK = 1210;
    public final static int REQUEST_SOLICITUD = 1211;

    @Override
    protected void onCreate(@Nullable  Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

    }




    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void ocultarTeclado() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (imm.isActive()) {
                            if (getCurrentFocus() != null) {
                                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                            }
                        }
                    }
                });
            }
        }).start();
    }

    protected void mostrarTeclado() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    protected void mostrarProgressDiealog(String mensaje) {
        if (pDialog != null) {
            if (pDialog.isShowing() && !isFinishing()) {
                pDialog.dismiss();
            }
        }
        if (!isFinishing()) {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage(mensaje);
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    protected void mostrarProgressDiealog(String mensaje, boolean cancelable) {
        if (pDialog != null) {
            if (pDialog.isShowing() && !isFinishing()) {
                pDialog.dismiss();
            }
        }

        if (!isFinishing()) {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage(mensaje);
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(cancelable);
            pDialog.show();
        }

    }

    protected void ocultarProgressDialog() {
        if (pDialog != null && !isFinishing()) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
        }
    }


    public void mensaje(String mensaje) {
        new AlertDialog.Builder(BaseActivity.this)
                .setTitle("Aviso")
                .setMessage(mensaje)
                .setCancelable(false)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }

    public void mensaje(String mensaje, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickCancelListener) {
        new AlertDialog.Builder(BaseActivity.this)
                .setTitle("Aviso")
                .setMessage(mensaje)
                .setCancelable(false)
                .setPositiveButton("Aceptar", onClickListener)
                .setNegativeButton("Cancelar", onClickCancelListener).show();
    }

    public void mensaje(String mensaje,final  boolean finish) {
        new AlertDialog.Builder(BaseActivity.this)
                .setTitle("Aviso")
                .setMessage(mensaje)
                .setCancelable(false)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(finish)finish();
                    }
                }).show();
    }

    public boolean validarNumeroTarjeta(String ccNumber) {

        try {
            ccNumber = ccNumber.replaceAll("\\D", "");
            char[] ccNumberArry = ccNumber.toCharArray();
            int checkSum = 0;
            for (int i = ccNumberArry.length - 1; i >= 0; i--) {
                char ccDigit = ccNumberArry[i];
                if ((ccNumberArry.length - i) % 2 == 0) {
                    int doubleddDigit = Character.getNumericValue(ccDigit) * 2;
                    checkSum += (doubleddDigit % 9 == 0 && doubleddDigit != 0) ? 9 : doubleddDigit % 9;
                } else {
                    checkSum += Character.getNumericValue(ccDigit);
                }
            }
            return (checkSum != 0 && checkSum % 10 == 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
