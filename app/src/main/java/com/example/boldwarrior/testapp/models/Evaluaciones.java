package com.example.boldwarrior.testapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class Evaluaciones implements Serializable {


    private Integer idEvaluacion;
    private Integer numero;
    private Integer opcion;
    private Integer estado;
    private String nombre;
    private String respuesta;
    private Date fecha;
    @SerializedName("body")
    private String text;

    //private String respuestas [];

    public Evaluaciones(Integer idEvaluacion, Integer numero, Integer opcion, Integer estado, String nombre, String respuesta, Date fecha, String text) {
        this.idEvaluacion = idEvaluacion;
        this.numero = numero;
        this.opcion = opcion;
        this.estado = estado;
        this.nombre = nombre;
        this.respuesta = respuesta;
        this.fecha = fecha;
        this.text = text;
    }

    public Integer getIdEvaluacion() {
        return idEvaluacion;
    }

    public void setIdEvaluacion(Integer idEvaluacion) {
        this.idEvaluacion = idEvaluacion;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Integer getOpcion() {
        return opcion;
    }

    public void setOpcion(Integer opcion) {
        this.opcion = opcion;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
