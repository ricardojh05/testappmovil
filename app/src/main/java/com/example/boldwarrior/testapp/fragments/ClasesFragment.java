package com.example.boldwarrior.testapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.boldwarrior.testapp.Api;
import com.example.boldwarrior.testapp.R;
import com.example.boldwarrior.testapp.activities.EvaluacionActivity;
import com.example.boldwarrior.testapp.models.Clases;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ClasesFragment extends Fragment {

    private Object AlumnosFragment;

    public ClasesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view= inflater.inflate(R.layout.fragment_clases, container, false);

         final ListView listView = (ListView) view.findViewById(R.id.listView);
            AlumnosFragment = new AlumnosFragment();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        Api api= retrofit.create(Api.class);
        Call<List<Clases>> call = api.getClases();
        call.enqueue(new Callback<List<Clases>>() {
            @Override
            public void onResponse(Call<List<Clases>> call, Response<List<Clases>> response) {
                final List<Clases> clases= response.body();

                String[] clasesNames =new String[clases.size()];
//                String[] idUsuario =new String[clases.size()];
                for (int i=0; i< clases.size(); i++){
                    clasesNames[i]=clases.get(i).getNombre_clase();
//                    idUsuario[i]=clases.get(i).getUserId();
//                    materiaNames[i]=clases.get(i).getMateria();
                }
                listView.setAdapter(
                        new ArrayAdapter<String>(
                                getActivity(),
                                android.R.layout.simple_list_item_1,
                                clasesNames
                        )
                );

                for (Clases h: clases){
                    //Log.d("id_clase",h.getId());
                    }

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent =new Intent(getActivity(), EvaluacionActivity.class);
                        intent.putExtra("objeto",clases.get(position));
                        startActivity(intent);
                    }
                });

            }

            @Override
            public void onFailure(Call<List<Clases>> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(),Toast.LENGTH_SHORT).show();
            }


        });
        return  view;
    }
}
