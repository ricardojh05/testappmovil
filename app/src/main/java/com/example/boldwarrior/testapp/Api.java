package com.example.boldwarrior.testapp;

import com.example.boldwarrior.testapp.models.Alumnos;
import com.example.boldwarrior.testapp.models.Clases;
import com.example.boldwarrior.testapp.models.Evaluaciones;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Api {
//    String BASE_URL ="https://simplifiedcoding.net/demos/";
    String BASE_URL ="http://testktaxi.kradac.com:3001/";

    //LISTA LAS CLASES
    @GET("clase")
    Call<List<Clases>> getClases();

    //@POST("evaluacion/")
    //Call<List<Evaluaciones>> getEvaluaciones();
    @FormUrlEncoded
    @POST("evaluacion")
    Call<List<Evaluaciones>> createPost(@Field("idClase") int idClase);

    @FormUrlEncoded
    @POST("alumno_eval")
    Call<List<Alumnos>> getAlumnos(@Field("idClase") int idClase,
                                   @Field("idEvaluacion") int idEvaluacion);

    @GET("clase")
    Call<List<Alumnos>> getAlumnos();
}