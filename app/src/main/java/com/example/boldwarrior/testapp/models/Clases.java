package com.example.boldwarrior.testapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Clases implements Serializable {
    //private String userId;
    private String nombre;
    private String color;
    //private String icono;
    private int idClase;
    private int idMateria;
    @SerializedName("body")
    private  String text;


    public Clases(int idClase, String nombre, int idMateria, String color) {
        this.idClase=idClase;
        this.idMateria=idMateria;
        this.color=color;
        //this.userId=userId;
        this.nombre=nombre;
        //cambio para probar coomit
    }

    public int getId(){
        return idClase;
    }

    public String getNombre_clase() {
        return nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getIdClase() {
        return idClase;
    }

    public void setIdClase(int idClase) {
        this.idClase = idClase;
    }

    public int getIdMateria() {
        return idMateria;
    }

    public void setIdMateria(int idMateria) {
        this.idMateria = idMateria;
    }


}
