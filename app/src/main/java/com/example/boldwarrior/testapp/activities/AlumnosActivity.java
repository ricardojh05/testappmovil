package com.example.boldwarrior.testapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.boldwarrior.testapp.Api;
import com.example.boldwarrior.testapp.R;
import com.example.boldwarrior.testapp.models.Alumnos;
import com.example.boldwarrior.testapp.models.Clases;
import com.example.boldwarrior.testapp.models.Evaluaciones;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class AlumnosActivity extends AppCompatActivity {

    private Clases Item;
    private Integer idClase;
    private Integer idEvaluacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alumnos);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent iin= getIntent();
        Bundle b = iin.getExtras();
        if(b!=null)
        {
            Evaluaciones evaluaciones=(Evaluaciones) b.getSerializable("objeto");
            idEvaluacion =evaluaciones.getIdEvaluacion();
            idClase =(Integer) b.get("idClase");
        }

        final ListView listView = (ListView) findViewById(R.id.listViewAlumnos);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api api= retrofit.create(Api.class);

        Call<List<Alumnos>> call = api.getAlumnos(idClase,idEvaluacion);
        call.enqueue(new Callback<List<Alumnos>>() {
            @Override
            public void onResponse(Call<List<Alumnos>> call, Response<List<Alumnos>> response) {
                Log.d("lista", String.valueOf(response));

                final List<Alumnos> alumnos= response.body();

                String[] evaluacionesNames =new String[alumnos.size()];
//                String[] idUsuario =new String[clases.size()];
                for (int i=0; i< alumnos.size(); i++){
                    evaluacionesNames[i]=alumnos.get(i).getNombre();
//                    idUsuario[i]=clases.get(i).getUserId();
//                    materiaNames[i]=clases.get(i).getMateria();
                }
                listView.setAdapter(new ArrayAdapter<String>(AlumnosActivity.this, android.R.layout.simple_list_item_1, evaluacionesNames));

                for (Alumnos h: alumnos){
                    //Log.d("id_clase",h.getId());
                }

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Integer idAlumno= alumnos.get(position).getIdAlumno();
                        Intent intent =new Intent(AlumnosActivity.this,OcrActivity.class);
                        Bundle extras = new Bundle();
                        extras.putInt("idEvaluacion",idEvaluacion);
                        extras.putInt("idClase",idClase);
                        extras.putInt("idAlumno",idAlumno);
                        intent.putExtras(extras);
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onFailure(Call<List<Alumnos>> call, Throwable t) {
                Toast.makeText(AlumnosActivity.this, t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }
}