package com.example.boldwarrior.testapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.boldwarrior.testapp.Api;
import com.example.boldwarrior.testapp.R;
import com.example.boldwarrior.testapp.models.Clases;
import com.example.boldwarrior.testapp.models.Evaluaciones;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EvaluacionActivity extends BaseActivity {

    private Clases Item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluacion);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent iin = getIntent();
        Bundle b = iin.getExtras();

        if (b != null) {
            final Clases j = (Clases) b.getSerializable("objeto");
            final ListView listView = (ListView) findViewById(R.id.listViewEvaluacion);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Api.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            Api api = retrofit.create(Api.class);

            mostrarProgressDiealog(getString(R.string.msg_cargando_evaluaciones));
            Call<List<Evaluaciones>> call = api.createPost(j.getIdClase());
            call.enqueue(new Callback<List<Evaluaciones>>() {
                @Override
                public void onResponse(Call<List<Evaluaciones>> call, Response<List<Evaluaciones>> response) {
                    ocultarProgressDialog();
                    Log.d("lista", String.valueOf(response));

                    final List<Evaluaciones> evaluaciones = response.body();

                    String[] evaluacionesNames = new String[evaluaciones.size()];
//                String[] idUsuario =new String[clases.size()];
                    for (int i = 0; i < evaluaciones.size(); i++) {
                        evaluacionesNames[i] = evaluaciones.get(i).getNombre();
//                    idUsuario[i]=clases.get(i).getUserId();
//                    materiaNames[i]=clases.get(i).getMateria();
                    }
                    listView.setAdapter(new ArrayAdapter<String>(EvaluacionActivity.this, android.R.layout.simple_list_item_1, evaluacionesNames));

                    for (Evaluaciones h : evaluaciones) {
                        //Log.d("id_clase",h.getId());
                    }

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(EvaluacionActivity.this, AlumnosActivity.class);
                            intent.putExtra("objeto",evaluaciones.get(position));
                            intent.putExtra("idClase",j.getIdClase());
                            startActivity(intent);

                        }
                    });

                }

                @Override
                public void onFailure(Call<List<Evaluaciones>> call, Throwable t) {
                    ocultarProgressDialog();
                    Toast.makeText(EvaluacionActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }


        // return  view;
    }


}

