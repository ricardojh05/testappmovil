package com.example.boldwarrior.testapp.fragments;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.boldwarrior.testapp.R;

public class EvaluacionesContenedorFragment extends Fragment{

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_contenedor_evaluaciones, container, false);

        return rootView;
    }
}
