package com.example.boldwarrior.testapp.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.boldwarrior.testapp.R;
import com.example.boldwarrior.testapp.tools.RequestPermissionsTool;
import com.example.boldwarrior.testapp.tools.RequestPermissionsToolImpl;
import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class OcrActivity extends Activity implements ActivityCompat.OnRequestPermissionsResultCallback  {

    private static final String TAG = MainActivity.class.getSimpleName();
    static final int PHOTO_REQUEST_CODE = 1;
    private TessBaseAPI tessBaseApi;
    private Integer idClase;
    private Integer idEvaluacion;
    private Integer idAlumno;
    TextView textView;
    Uri outputFileUri;
    private static final String lang = "eng";
    String result = "empty";
    private RequestPermissionsTool requestTool; //for API >=23 only

    private static final String DATA_PATH = Environment.getExternalStorageDirectory().toString() + "/TesseractSample/";
    private static final String TESSDATA = "tessdata";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent iin= getIntent();
        Bundle b = iin.getExtras();
        if(b!=null)
        {
            idEvaluacion =(Integer) b.get("idEvaluacion");
            idClase =(Integer) b.get("idClase");
            idAlumno =(Integer) b.get("idAlumno");
        }


        Button captureImg = (Button) findViewById(R.id.action_btn);
        if (captureImg != null) {
            captureImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startCameraActivity();
                }
            });
        }
        textView = (TextView) findViewById(R.id.textResult);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions();
        }

        Button btn_corregir=findViewById(R.id.btn_corregir);
        btn_corregir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogModificar();
            }
        });
        Button btn_guardar=findViewById(R.id.btn_guardar);
        btn_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    /**
     * to get high resolution image from camera
     */
    private void startCameraActivity() {
        try {
            String IMGS_PATH = Environment.getExternalStorageDirectory().toString() + "/TesseractSample/imgs";
            prepareDirectory(IMGS_PATH);

            String img_path = IMGS_PATH + "/ocr.jpg";

           /*  outputFileUri = FileProvider.getUriForFile(
                    OcrActivity.this,
                    "com.example.boldwarrior.testapp.provider", //(use your app signature + ".provider" )
                   new File(img_path));
*/
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());

            outputFileUri = Uri.fromFile(new File(img_path));
            //this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

            final Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, PHOTO_REQUEST_CODE);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent data) {
        //making photo
        if (requestCode == PHOTO_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            doOCR();
        } else {
            Toast.makeText(this, "ERROR: Image was not obtained.", Toast.LENGTH_SHORT).show();
        }
    }

    private void doOCR() {
        prepareTesseract();
        startOCR(outputFileUri);
    }

    /**
     * Prepare directory on external storage
     *
     * @param path
     * @throws Exception
     */
    private void prepareDirectory(String path) {

        File dir = new File(path);
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                Log.e(TAG, "ERROR: Creation of directory " + path + " failed, check does Android Manifest have permission to write to external storage.");
            }
        } else {
            Log.i(TAG, "Created directory " + path);
        }
    }


    private void prepareTesseract() {
        try {
            prepareDirectory(DATA_PATH + TESSDATA);
        } catch (Exception e) {
            e.printStackTrace();
        }

        copyTessDataFiles(TESSDATA);
    }

    /**
     * Copy tessdata files (located on assets/tessdata) to destination directory
     *
     * @param path - name of directory with .traineddata files
     */
    private void copyTessDataFiles(String path) {
        try {
            String fileList[] = getAssets().list(path);

            for (String fileName : fileList) {

                // open file within the assets folder
                // if it is not already there copy it to the sdcard
                String pathToDataFile = DATA_PATH + path + "/" + fileName;
                if (!(new File(pathToDataFile)).exists()) {

                    InputStream in = getAssets().open(path + "/" + fileName);

                    OutputStream out = new FileOutputStream(pathToDataFile);

                    // Transfer bytes from in to out
                    byte[] buf = new byte[1024];
                    int len;

                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    in.close();
                    out.close();

                    Log.d(TAG, "Copied " + fileName + "to tessdata");
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "Unable to copy files to tessdata " + e.toString());
        }
    }


    /**
     * don't run this code in main thread - it stops UI thread. Create AsyncTask instead.
     * http://developer.android.com/intl/ru/reference/android/os/AsyncTask.html
     *
     * @param imgUri
     */
    private void startOCR(Uri imgUri) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4; // 1 - means max size. 4 - means maxsize/4 size. Don't use value <4, because you need more memory in the heap to store your data.
            Bitmap bitmap = BitmapFactory.decodeFile(imgUri.getPath(), options);

            result = extractText(bitmap);
            String      textResult="1.   "+ "<font color='#FF0000'> A </font>" +" B "+" C "+" D "+"<br>";
            textResult=textResult+"2.   "+" A "+" <font color='#FF0000'> B </font> "+" C "+" D "+"<br>";
            textResult=textResult+"3.   "+" <font color='#FF0000'> A </font> "+" B "+" C "+" D "+"<br>";
            textResult=textResult+"4.   "+" A "+" B "+" <font color='#FF0000'> C </font> "+" D "+"<br>";
            textResult=textResult+"5.   "+" A "+" <font color='#FF0000'> B </font> "+" C "+" D "+"<br>";
            textResult=textResult+"6.   "+" A "+" B "+" <font color='#FF0000'> C </font> "+" D "+"<br>";
            textResult=textResult+"7.   "+" <font color='#FF0000'> A </font>"+" B "+" C "+" D "+"<br>";
            textResult=textResult+"8.   "+" A "+" <font color='#FF0000'> B </font> "+" C "+" D "+"<br>";
            textResult=textResult+"9.   "+" A "+" 8 "+" O "+" D "+"<br>";
            textResult=textResult+"10. "+" <font color='#FF0000'> A </font> "+" B "+" C "+" D "+"<br>";
            textView.setText(Html.fromHtml(textResult));
            findViewById(R.id.btn_corregir).setVisibility(View.VISIBLE);
            Log.e(TAG,result);

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }


    private String extractText(Bitmap bitmap) {
        try {
            tessBaseApi = new TessBaseAPI();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            if (tessBaseApi == null) {
                Log.e(TAG, "TessBaseAPI is null. TessFactory not returning tess object.");
            }
        }

        tessBaseApi.init(DATA_PATH, lang);

//       //EXTRA SETTINGS
//        //For example if we only want to detect numbers
//        tessBaseApi.setVariable(TessBaseAPI.VAR_CHAR_WHITELIST, "1234567890");
//
//        //blackList Example
//        tessBaseApi.setVariable(TessBaseAPI.VAR_CHAR_BLACKLIST, "!@#$%^&*()_+=-qwertyuiop[]}{POIU" +
//                "YTRWQasdASDfghFGHjklJKLl;L:'\"\\|~`xcvXCVbnmBNM,./<>?");

        Log.d(TAG, "Training file loaded");
        tessBaseApi.setImage(bitmap);
        String extractedText = "empty result";
        try {
            extractedText = tessBaseApi.getUTF8Text();
        } catch (Exception e) {
            Log.e(TAG, "Error in recognizing text.");
        }
        tessBaseApi.end();
        return extractedText;
    }


    private void requestPermissions() {
        String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
        requestTool = new RequestPermissionsToolImpl();
        requestTool.requestPermissions(this, permissions);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        boolean grantedAllPermissions = true;
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                grantedAllPermissions = false;
            }
        }

        if (grantResults.length != permissions.length || (!grantedAllPermissions)) {

            requestTool.onPermissionDenied();
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    AlertDialog alertDialog;
    public void dialogModificar() {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_modificar_texto, null);
        final Button btnAceptar = v.findViewById(R.id.btnAceptar);

        final EditText textRespuesta = v.findViewById(R.id.txt_respuesta);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String      textResult="1.   "+ "<font color='#FF0000'> A </font>" +" B "+" C "+" D "+"<br>";
                textResult=textResult+"2.   "+" A "+" <font color='#FF0000'> B </font> "+" C "+" D "+"<br>";
                textResult=textResult+"3.   "+" <font color='#FF0000'> A </font> "+" B "+" C "+" D "+"<br>";
                textResult=textResult+"4.   "+" A "+" B "+" <font color='#FF0000'> C </font> "+" D "+"<br>";
                textResult=textResult+"5.   "+" A "+" <font color='#FF0000'> B </font> "+" C "+" D "+"<br>";
                textResult=textResult+"6.   "+" A "+" B "+" <font color='#FF0000'> C </font> "+" D "+"<br>";
                textResult=textResult+"7.   "+" <font color='#FF0000'> A </font>"+" B "+" C "+" D "+"<br>";
                textResult=textResult+"8.   "+" A "+" <font color='#FF0000'> B </font> "+" C "+" D "+"<br>";
                textResult=textResult+"9.   "+" A "+" B "+" <font color='#FF0000'> C </font> "+" D "+"<br>";
                textResult=textResult+"10. "+" <font color='#FF0000'> A </font> "+" B "+" C "+" D "+"<br>";
                textView.setText(Html.fromHtml(textResult));

                findViewById(R.id.btn_corregir).setVisibility(View.GONE);
                findViewById(R.id.btn_guardar).setVisibility(View.VISIBLE);
                alertDialog.dismiss();
            }
        });
        localBuilder.setCancelable(false);
        localBuilder.setView(v);
        alertDialog=localBuilder.create();
        alertDialog.show();
    }
}


